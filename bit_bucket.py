#!/usr/bin/python3
# Copyright Tomasz Piersa tpiersa[at]gmail.com
#
import requests
import json
import sys
import pprint
from collections import defaultdict
user={}
my_project=sys.argv[1]
user[my_project]={}

url="https://api.bitbucket.org/2.0/repositories/"+my_project
response = requests.get(url, verify=False)
data = response.json()
#get repository name
repo_info={}
commit_links=[]
#main
for i in range(0, len(data["values"])):
    for value in data["values"][i]["links"]["commits"].values():
        commit_links.append(value)
        repo_info[data["values"][i]["name"]]=(value, data["values"][i]["scm"])

##get commits
for r_name in repo_info.keys():
    #repo_info[r_name]
    user[my_project][r_name]={}
    #temp=user[my_project][r_name]
    url=repo_info[r_name][0]
    response = requests.get(url, verify=False)
    data = response.json()
    commit_by_person=[]
#all persons commited in 
    for i in range (0, len(data["values"])):
        if len(data["values"][i]["author"]) <= 1: 
            continue
        commit_by_person.append(data["values"][i]["author"]["user"]["display_name"] )
#count quantity
    d=defaultdict(int)
    for name in commit_by_person:
        d[name] += 1
    for k in d.keys():
#calculate percent
        perc=((d[k]/len(commit_by_person))*100)
        user[my_project][r_name].update({k:perc})
pprint.pprint(user)
